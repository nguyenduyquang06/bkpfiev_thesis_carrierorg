#!/bin/sh
GAE_PRJ_ID="quang-personal"
GAE_APP_YML="app.yaml"
GAE_DEPLOY_VERSION="1.0"
GAE_URL="https://${GAE_DEPLOY_VERSION}-dot-${GAE_PRJ_ID}.appspot.com"

yarn build

if [ $? -ne 0 ]; then
  echo "yarn build failed."
  exit 1
fi

echo "Y" | \
  gcloud --project $GAE_PRJ_ID \
  app \
  deploy $GAE_APP_YML \
  --version $GAE_DEPLOY_VERSION \
  --no-promote

open $GAE_URL
