const express = require("express")
const next = require("next")
const compression = require("compression")
const moment = require("moment")
const cookieParser = require("cookie-parser")
const bodyParser = require("body-parser")
require("dotenv").config()
const dev = process.env.NODE_ENV !== "production"
const app = next({
  dev,
})
const handle = app.getRequestHandler()
const port = dev ? process.env.DEV_WEB_PORT : process.env.WEB_PORT
app
  .prepare()
  .then(() => {
    const server = express()
    server.use(bodyParser.json())
    server.use(cookieParser())
    // support gzip
    server.use(compression())

    server.post("/setCookie", (req, res) => {
      const {
        token
      } = req.body
      if (token) {
        res.cookie("token", token, {
          httpOnly: false,
          encode: String,
          expires: moment()
            .add(7, "day")
            .toDate(),
        })
      }
      res.status(200).json({
        message: "Set cookie successfully!",
      })
    })

    server.get("/get", (req, res) => {
      res.status(200).json({
        message: "Done!",
      })
    })

    server.get("*", (req, res) => {
      return handle(req, res)
    })

    server.listen(port, err => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
      console.log(`> Server API: ${process.env.API_END_POINT}`)
    })
  })
  .catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })
