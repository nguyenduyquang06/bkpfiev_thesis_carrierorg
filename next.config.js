const withTypescript = require("@zeit/next-typescript")
const withCSS = require("@zeit/next-css")

module.exports = withTypescript(
  withCSS({
    publicRuntimeConfig: {
      // Will be available on both server and client
      API_END_POINT: process.env.API_END_POINT || "http://localhost:4000",
    },
  })
)
