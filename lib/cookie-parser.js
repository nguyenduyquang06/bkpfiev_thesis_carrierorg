import Cookies from "universal-cookie";

export default function (context = {}) {
  const {
    req
  } = context;
  // get cookie from SS
  if (req) {
    return req.cookies;
  }
  // get cookie from CS
  const cookies = new Cookies()
  return cookies.getAll()
  // if (typeof window !== 'undefined') {
  //   const cookies = document.cookie.split(';');
  //   return loFromPairs(cookies.map(c => c.trim().split('=')));
  // }
}
