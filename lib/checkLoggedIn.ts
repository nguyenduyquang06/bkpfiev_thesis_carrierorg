import redirect from "../lib/redirect"
import { checkAuthorized } from "../service/auth"
import { AuthActions } from "../store/actions/AuthAction"
export default async function(ctx) {
  try {
    const { store } = ctx
    // const {
    //   auth: { currentUser },
    // } = store.getState()
    // if (currentUser && currentUser.token) {
    //   // console.log(currentUser)
    //   return
    // }
    const user = await checkAuthorized(ctx)
    if (!user) {
      redirect(ctx, "/auth/sign-in")
      throw new Error("Cant request data")
    }
    store.dispatch({
      type: AuthActions.loginUserSuccess.toString(),
      payload: {
        user,
      },
    })
  } catch (e) {
    // already auto redirected to log in :D
    // redirect(context, '/auth/sign-in');
    throw e
  }
}
