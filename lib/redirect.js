import Router from 'next/router';
import loGet from 'lodash/get';

export default (context, target) => {
  const currentPath = loGet(context, 'req.path') || loGet(window, 'location.pathname');
  if (currentPath === target) {
    return;
  }

  const isServer = !!loGet(context, 'req');
  if (isServer) {
    context.res.redirect(target);
  } else {
    // window.location.href = target;
    Router.replace(target);
  }
};
