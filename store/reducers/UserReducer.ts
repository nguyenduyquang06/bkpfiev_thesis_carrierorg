import produce from "immer"
import { reducerWithInitialState } from "typescript-fsa-reducers"
import MockAccount from "../../constants/mockAccount"
import { login } from "../../service/auth"
import { IUserPayload, UserAction } from "../actions/UserAction"
import { IUserState, UserInitialState } from "../states"

export const userReducers = reducerWithInitialState(UserInitialState)
  .case(UserAction.login, (state: IUserState, _: IUserPayload) => {
    return produce(state, draft => {
      draft.count = state.count + 1
    })
  })
  .case(UserAction.logout, (state: IUserState, _: IUserPayload) => {
    return produce(state, draft => {
      draft.count = state.count - 1
    })
  })
  .case(UserAction.getToken, (state: IUserState, _: IUserPayload) => {
    return produce(state, draft => {
      login(MockAccount.username, MockAccount.password)
      draft.count = state.count - 1
    })
  })
