import produce from "immer"
import { reducerWithInitialState } from "typescript-fsa-reducers"
import {
  CoffeeBatchActions,
  ICoffeeError,
  ICoffeePayload,
} from "../actions/CoffeeBatchAction"
import { CoffeeBatchInitialState, ICoffeeBatchState } from "../states"

export const coffeeBatchReducers = reducerWithInitialState(
  CoffeeBatchInitialState
)
  .case(
    CoffeeBatchActions.getAllCoffeeBatchSuccess,
    (state: ICoffeeBatchState, payload: ICoffeePayload) => {
      return produce(state, draft => {
        draft.coffeeBatchList = JSON.parse(payload.message)
        draft.success = payload.success
      })
    }
  )
  .case(
    CoffeeBatchActions.getAllCoffeeBatchFail,
    (state: ICoffeeBatchState, err: ICoffeeError) => {
      // const res = await login(MockAccount.username, MockAccount.password)
      return produce(state, draft => {})
    }
  )
