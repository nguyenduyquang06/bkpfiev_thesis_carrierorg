import produce from "immer"
import { reducerWithInitialState } from "typescript-fsa-reducers"
import { AuthActions, IAuthError, IAuthPayload } from "../actions/AuthAction"
import { AuthInitialState, IAuthState } from "../states"

export const authReducers = reducerWithInitialState(AuthInitialState)
  .case(
    AuthActions.loginUserSuccess,
    (state: IAuthState, payload: IAuthPayload) => {
      // const res = await login(MockAccount.username, MockAccount.password)
      return produce(state, draft => {
        draft.isLoggedIn = true
        draft.error = false
        draft.currentUser = payload
      })
    }
  )
  .case(AuthActions.loginUserFail, (state: IAuthState, err: IAuthError) => {
    // const res = await login(MockAccount.username, MockAccount.password)
    return produce(state, draft => {
      draft.isLoggedIn = false
      draft.error = true
      draft.currentUser = {}
    })
  })
