import { Page } from "../constants"

/**
 * Coffee batch
 */
export interface ICoffeeBatchState {
  coffeeBatchList: Array<object>
  success: boolean
}
export const CoffeeBatchInitialState: ICoffeeBatchState = {
  coffeeBatchList: [],
  success: false,
}

/**
 * Authentication
 */
export interface IAuthState {
  isLoggedIn: boolean
  error: boolean
  currentUser: object
}
export const AuthInitialState: IAuthState = {
  isLoggedIn: false,
  error: false,
  currentUser: {},
}

/**
 * User details
 */
export interface IUserState {
  count: number
}
export const UserInitialState: IUserState = {
  count: 1,
}

/**
 * Counter
 */
export interface ICounterState {
  count: number
}
export const CounterInitialState: ICounterState = {
  count: 1,
}

/**
 * Page
 */
export interface IPageState {
  selectedPage: Page
}
export const PageInitialState: IPageState = {
  selectedPage: Page.Index,
}

/**
 * Initial state tree interface
 */
export interface IInitialState {
  /**
   * Root counter state
   */
  counter: ICounterState
  /**
   * Root page state
   */
  page: IPageState
  /**
   * Root user state
   */
  user: IUserState
  /**
   * Auth user state
   */
  auth: IAuthState
  /**
   * coffee batch state
   */
  coffeeBatch: ICoffeeBatchState
}

/**
 * Initial state tree
 */
export const InitialState: IInitialState = {
  /**
   * Root counter state
   */
  counter: CounterInitialState,
  /**
   * Root page state
   */
  page: PageInitialState,
  /**
   * Root user state
   */
  user: UserInitialState,
  /**
   * Auth user state
   */
  auth: AuthInitialState,
  /**
   * coffee batch state
   */
  coffeeBatch: CoffeeBatchInitialState,
}
