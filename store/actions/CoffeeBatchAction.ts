import actionCreatorFactory from "typescript-fsa"
import { getAllCoffeeBatch } from "../../service/coffeeBatch"
const actionCreator = actionCreatorFactory("coffeeBatch")

export interface ICoffeePayload {
  success: boolean
  message: Array<object>
}

export interface ICoffeeError {
  error: any
}

const getAllCoffeeBatchDispatch = async ctx => {
  try {
    const payload = await getAllCoffeeBatch(ctx)
    ctx.store.dispatch({
      type: CoffeeBatchActions.getAllCoffeeBatchSuccess.toString(),
      payload,
    })
  } catch (e) {
    ctx.store.dispatch({
      type: CoffeeBatchActions.getAllCoffeeBatchFail.toString(),
      error: e,
    })
    throw e
  }
}

function getAllCoffeeBatchDPClient() {
  return async dispatch => {
    try {
      const payload = await getAllCoffeeBatch()
      return dispatch({
        type: CoffeeBatchActions.getAllCoffeeBatchSuccess.toString(),
        payload,
      })
    } catch (e) {
      return dispatch({
        type: CoffeeBatchActions.getAllCoffeeBatchFail.toString(),
        error: e,
      })
    }
  }
}

export const CBActionImplement = {
  getAllCoffeeBatchDispatch,
  getAllCoffeeBatchDPClient,
}

export const CoffeeBatchActions = {
  getAllCoffeeBatchSuccess: actionCreator<ICoffeePayload>(
    "getAllCoffeeBatchSuccess"
  ),
  getAllCoffeeBatchFail: actionCreator<ICoffeeError>("getAllCoffeeBatchFail"),
}
