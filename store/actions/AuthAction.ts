import actionCreatorFactory from "typescript-fsa"
import { login } from "../../service/auth"
const actionCreator = actionCreatorFactory("auth")

export interface IAuthPayload {
  username: string
  password: string
}

export interface IAuthError {
  error: any
}

function loginUser(username, pwd) {
  return async dispatch => {
    try {
      const payload = await login(username, pwd)
      return dispatch({
        type: AuthActions.loginUserSuccess.toString(),
        payload,
      })
    } catch (e) {
      return dispatch({
        type: AuthActions.loginUserFail.toString(),
        error: e,
      })
    }
  }
}

export const AuthActionImplement = {
  loginUser,
}

export const AuthActions = {
  loginUserSuccess: actionCreator<IAuthPayload>("loginUserSuccess"),
  loginUserFail: actionCreator<IAuthError>("loginUserFail"),
  logoutUser: actionCreator<IAuthPayload>("logoutUser"),
}
