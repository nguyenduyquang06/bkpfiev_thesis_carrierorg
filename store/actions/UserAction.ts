import actionCreatorFactory from "typescript-fsa"

const actionCreator = actionCreatorFactory("user")

export interface IUserPayload {
  inputNumber: number
}

export const UserAction = {
  login: actionCreator<IUserPayload>("login"),
  logout: actionCreator<IUserPayload>("logout"),
  getToken: actionCreator<IUserPayload>("getToken"),
}
