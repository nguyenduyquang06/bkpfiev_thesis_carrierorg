import { applyMiddleware, createStore } from "redux";
import thunkMiddleware from 'redux-thunk';
import { InitialState } from "../store/states";
import { combinedReducers } from "./reducers";

export function configureStore(initialState = InitialState) {
  return createStore(combinedReducers, initialState, applyMiddleware(thunkMiddleware))
}
