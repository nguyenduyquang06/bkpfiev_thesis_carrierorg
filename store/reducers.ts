import { combineReducers } from "redux"
import { authReducers } from "./reducers/AuthReducer"
import { coffeeBatchReducers } from "./reducers/CoffeeBatchReducer"
import { countReducers } from "./reducers/CounterReducer"
import { pageReducers } from "./reducers/PageReducer"
import { userReducers } from "./reducers/UserReducer"
import { IInitialState } from "./states"

export const combinedReducers = combineReducers<IInitialState>({
  counter: countReducers,
  page: pageReducers,
  user: userReducers,
  auth: authReducers,
  coffeeBatch: coffeeBatchReducers,
})
