import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Typography from "@material-ui/core/Typography"
import React from "react"

function PaymentForm() {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Confirmation key
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12} md={12}>
          <TextField
            required
            id="confirmationKey"
            label="Confirmation Key"
            fullWidth
          />
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

export default PaymentForm
