import IconButton from "@material-ui/core/IconButton"
import InputBase from "@material-ui/core/InputBase"
import Menu from "@material-ui/core/Menu"
import MenuItem from "@material-ui/core/MenuItem"
import Paper from "@material-ui/core/Paper"
import { createStyles, Theme, withStyles } from "@material-ui/core/styles"
import MenuIcon from "@material-ui/icons/Menu"
import SearchIcon from "@material-ui/icons/Search"
import _ from "lodash"
import React from "react"

interface IProps {
  classes: object
}

interface IState {}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      padding: "2px 4px",
      display: "flex",
      alignItems: "center",
      width: 400,
    },
    input: {
      marginLeft: 8,
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      width: 1,
      height: 28,
      margin: 4,
    },
  })

const ListFilter = [
  {
    key: "growerMSPID",
    label: "Grower MSPID",
  },
  {
    label: "Batch ID",
    key: "batchID",
  },
]

class SearchBar extends React.Component<IProps, IState> {
  state = {
    anchorEl: null,
    selectedFilter: "batchID",
  }
  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget })
  }
  handleClose = e => {
    console.log(e.target.id)
    this.setState({ anchorEl: null, selectedFilter: e.target.id })
  }
  render() {
    const { classes } = this.props
    const { anchorEl } = this.state
    return (
      <Paper className={classes.root} elevation={1}>
        <IconButton
          className={classes.iconButton}
          aria-owns={anchorEl ? "simple-menu" : undefined}
          aria-haspopup="true"
          onClick={this.handleClick}
          aria-label="Menu"
        >
          <MenuIcon />
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {_.map(ListFilter, o => {
            return (
              <MenuItem id={o.key} key={o.key} onClick={this.handleClose}>
                {o.label}
              </MenuItem>
            )
          })}
        </Menu>
        <InputBase
          className={classes.input}
          placeholder="Search coffee batch"
        />
        <IconButton className={classes.iconButton} aria-label="Search">
          <SearchIcon />
        </IconButton>
      </Paper>
    )
  }
}

export default withStyles(styles)(SearchBar as any)
