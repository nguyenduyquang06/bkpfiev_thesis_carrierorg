import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import React from 'react';

interface IProps {
  open: boolean
  fullScreen: boolean
  handleClose: () => void
  coffeeBatch: {
    batchID: string
    batchState: string
    beanSize: string
    roastType: string
    growerID: string
    growerMSPID: string
    cropDate: string
    amount: string
    owner: string
    cropID: string
    blockID: string
    fairTradeReport: {
      reportName: string
      organizationDescription: string
      reportYear: string
      fairtradePremiumInvested: string
      investmentTitle1: string
      investmentAmount1: string
      investmentTitle2: string
      investmentAmount2: string
      investmentTitle3: string
      investmentAmount3: string
    }
  }
}

interface IState {

}



class ViewCoffeeBatchDetails extends React.Component<IProps, IState> {
  render() {
    const {
      fullScreen,
      open,
      handleClose,
      coffeeBatch
    } = this.props;
    return (
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">Coffee Batch Details: {coffeeBatch.batchID}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <strong>Batch ID:</strong> {coffeeBatch.batchID}
          </DialogContentText>
          <DialogContentText>
            <strong>Batch State:</strong> {coffeeBatch.batchState}
          </DialogContentText>
          <DialogContentText>
            <strong>Bean size: </strong> {coffeeBatch.beanSize}
          </DialogContentText>
          <DialogContentText>
            <strong>Grower ID:</strong> {coffeeBatch.growerID}
          </DialogContentText>
          <DialogContentText>
            <strong>Grower Organization ID:</strong> {coffeeBatch.growerMSPID}
          </DialogContentText>
          <DialogContentText>
            <strong>Cropped Date:</strong> {coffeeBatch.cropDate}
          </DialogContentText>
          <DialogContentText>
            <strong>Owner:</strong> {coffeeBatch.owner}
          </DialogContentText>
          <DialogContentText>
            <strong>Amount:</strong> {coffeeBatch.amount}
          </DialogContentText>
          <DialogContentText>
            <strong>Crop ID:</strong> {coffeeBatch.cropID}
          </DialogContentText>
          <DialogContentText>
            <strong>Block ID:</strong> {coffeeBatch.blockID}
          </DialogContentText>
          <DialogContentText>
            <strong>Roast Type:</strong>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withMobileDialog()(ViewCoffeeBatchDetails as any);
