import AppBar from "@material-ui/core/AppBar"
import Button from "@material-ui/core/Button"
import Dialog from "@material-ui/core/Dialog"
import Divider from "@material-ui/core/Divider"
import Grid from "@material-ui/core/Grid"
import IconButton from "@material-ui/core/IconButton"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import Slide from "@material-ui/core/Slide"
import { withStyles } from "@material-ui/core/styles"
import Toolbar from "@material-ui/core/Toolbar"
import Typography from "@material-ui/core/Typography"
import CloseIcon from "@material-ui/icons/Close"
import GoogleMapReact from "google-map-react"
import _ from "lodash"
import moment from "moment"
import PropTypes from "prop-types"
import React from "react"
import ReactTable from "react-table"
import Marker from "../atoms/Marker"

const styles = {
  appBar: {
    position: "relative",
  },
  flex: {
    flex: 1,
  },
}
const AnyReactComponent = ({ text }) => <div>{text}</div>
function Transition(props) {
  return <Slide direction="left" {...props} />
}

const centerPoint = coffeeList => {
  const lat = _.meanBy(coffeeList, o => {
    return o.latitude
  })

  const lng = _.meanBy(coffeeList, o => {
    return o.longitude
  })

  return {
    lat,
    lng,
  }
}

class CoffeeBatchTrackingDetailsDialog extends React.Component {
  static defaultProps = {
    zoom: 14,
  }

  constructor(props) {
    super(props)
    this.state = {
      centerPoint: centerPoint(props.coffeeBatch),
    }
  }

  render() {
    const { classes, open, handleClose, coffeeBatch } = this.props
    const { centerPoint } = this.state
    return (
      <div>
        <Dialog
          fullScreen
          open={open}
          onClose={handleClose}
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={handleClose}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.flex}>
                View Coffee Batch Tracking Details
              </Typography>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleClose}
              >
                CLOSE
              </Button>
            </Toolbar>
          </AppBar>
          <Grid style={{ padding: "20px" }} container spacing={8}>
            <Grid item xs={4}>
              <div align="center">
                <Typography
                  component={"span"}
                  gutterBottom
                  variant="h4"
                  color="inherit"
                >
                  Coffee Batch Details
                </Typography>
              </div>
              <List>
                <ListItem>
                  <ListItemText primary="Batch ID" secondary="Updating..." />
                </ListItem>
                <Divider />
                <ListItem>
                  <ListItemText primary="Batch State" secondary="Updating..." />
                </ListItem>
              </List>
            </Grid>
            <Grid item xs={8}>
              <div align="center">
                <Typography
                  component={"span"}
                  gutterBottom
                  variant="h4"
                  color="inherit"
                >
                  Tracking
                </Typography>
              </div>
              <div align="center" style={{ height: "50vh", width: "100%" }}>
                <GoogleMapReact
                  bootstrapURLKeys={{
                    key:
                      "AIzaSyA7Vzoo-bz-oYmSeSBqNkREYJ2flzSqZiA" /* YOUR KEY HERE */,
                  }}
                  defaultCenter={centerPoint}
                  defaultZoom={this.props.zoom}
                >
                  {_.map(coffeeBatch, o => {
                    return (
                      <Marker
                        lng={o.longitude}
                        lat={o.latitude}
                        text="My Marker"
                      />
                    )
                  })}
                </GoogleMapReact>
                <ReactTable
                  data={coffeeBatch || []}
                  SubComponent={row => {
                    return (
                      <span>
                        <Button color="primary">Get Address</Button>
                        <strong>Address:</strong>
                        <br />
                      </span>
                    )
                  }}
                  columns={[
                    {
                      Header: () => (
                        <Typography
                          component={"span"}
                          gutterBottom
                          variant="h5"
                          color="inherit"
                        >
                          <strong>Sensor Update History</strong>
                        </Typography>
                      ),
                      columns: [
                        {
                          Header: () => <strong>Temperature</strong>,
                          id: "temperature",
                          accessor: d => d.temperature,
                        },
                        {
                          Header: () => <strong>Hudmidtiy</strong>,
                          id: "humidity",
                          accessor: d => d.humidity,
                        },
                        {
                          Header: () => <strong>Latitude</strong>,
                          id: "latitude",
                          accessor: d => d.latitude,
                        },
                        {
                          Header: () => <strong>Longitude</strong>,
                          id: "longitude",
                          accessor: d => d.longitude,
                        },
                        {
                          Header: () => <strong>Country</strong>,
                          id: "country",
                          accessor: d => d.country,
                        },
                        {
                          Header: () => <strong>Time</strong>,
                          id: "rfcTime",
                          accessor: d =>
                            moment(d.rfcTime).format("YYYY-MM-DD HH:mm:ss"),
                        },
                      ],
                    },
                  ]}
                  defaultPageSize={5}
                />
              </div>
            </Grid>
          </Grid>
        </Dialog>
      </div>
    )
  }
}

CoffeeBatchTrackingDetailsDialog.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(CoffeeBatchTrackingDetailsDialog)
