import { pink } from "@material-ui/core/colors"
import grey from "@material-ui/core/colors/grey"
/**
 * material-ui theme color pallete
 * @see https://material-ui.com/style/color/
 */
export const palette = {
  typography: {
    // https://material-ui.com/style/typography/#strategies
    useNextVariants: true,
  },
  primary: {
    light: grey[600],
    main: grey[800],
    dark: grey[900],
  },
  secondary: pink,
}
