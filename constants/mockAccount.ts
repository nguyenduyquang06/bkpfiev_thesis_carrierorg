const Account = {
  testAccount: {
    orgName: "carrierorg",
    username: "test01",
    password: "test",
  },
  adminAccount: {
    orgName: "carrierorg",
    username: "admin",
    password: "adminpw",
  },
}

const MAINACCOUNT = Account.adminAccount
export default MAINACCOUNT
