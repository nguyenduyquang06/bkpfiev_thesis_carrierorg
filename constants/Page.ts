import { Color } from "@material-ui/core"
import {
  blue,
  green,
  indigo,
  orange,
  pink,
  purple,
  teal,
} from "@material-ui/core/colors"
import { SvgIconProps } from "@material-ui/core/SvgIcon"
import { CompareArrows } from "@material-ui/icons"
import HomeIcon from "@material-ui/icons/Home"
import InfoIcon from "@material-ui/icons/Info"
import CoffeeIcon from "@material-ui/icons/Subject"
import UserIcon from "@material-ui/icons/SupervisedUserCircle"
import TrackChanges from "@material-ui/icons/TrackChanges"
import WorkIcon from "@material-ui/icons/Work"
import { IEnum } from "."

/**
 * Page constants
 * @author tree
 */
export class Page implements IEnum<Page> {
  /**
   * For values() array
   */
  private static _values = new Array<Page>()

  public static readonly Index = new Page(
    1,
    "Welcome",
    "Welcome page",
    "Welcome page",
    "Welcome to Bach Khoa PFIEV Blockchain system",
    "/",
    HomeIcon,
    pink
  )
  public static readonly COMPANY = new Page(
    2,
    "Company",
    "Company information",
    "User details page",
    "User testing page.",
    "/company",
    WorkIcon,
    indigo
  )
  public static readonly BATCH = new Page(
    3,
    "Coffee batch",
    "Interact coffee batch transaction, confirm, accept request coffee batch transaction",
    "Coffee batch",
    "Interact with list of coffee batch transaction, confirm, accept request coffee batch transaction",
    "/coffeebatch",
    CoffeeIcon,
    purple
  )
  public static readonly CONTRACT = new Page(
    4,
    "Contract",
    "Interact contract transaction, confirm, accept request contract",
    "Contract",
    "Interact contract transaction, confirm, accept request contract",
    "/contract",
    CompareArrows,
    teal
  )
  public static readonly SENSORINFO = new Page(
    5,
    "Tracking",
    "Track coffee batch using Smart Storage Box or Container",
    "Smart Storage",
    "Manage and track coffee batch by smart storage",
    "/sensorinfo",
    TrackChanges,
    green
  )
  public static readonly USER = new Page(
    6,
    "User and Balance",
    "User and Balance",
    "User details page",
    "User details and balance ETH,...",
    "/user",
    UserIcon,
    blue
  )
  public static readonly ABOUT = new Page(
    7,
    "About",
    "About this site",
    "About | Author",
    "Site about page.",
    "/about",
    InfoIcon,
    orange
  )

  /**
   * constructor
   * @param number page id
   * @param pageTitle page title
   * @param pageDescription page description
   * @param title seo title
   * @param metaDescription seo meta description
   * @param relativeUrl relative url
   * @param icon page icon
   * @param iconColor page icon color
   */
  private constructor(
    public readonly id: number,
    public readonly pageTitle: string,
    public readonly pageDescription: string,
    public readonly title: string,
    public readonly metaDescription: string,
    public readonly relativeUrl: string,
    public readonly icon: React.ComponentType<SvgIconProps>,
    public readonly iconColor: Color
  ) {
    Page._values.push(this)
  }

  /**
   * Instance array
   */
  static get values(): Page[] {
    return this._values
  }

  /**
   * @inheritdoc
   */
  equals = (target: Page): boolean => this.id === target.id

  /**
   * @inheritdoc
   */
  toString = (): string =>
    `${this.id}, ${this.pageTitle}, ${this.pageDescription}`
}
