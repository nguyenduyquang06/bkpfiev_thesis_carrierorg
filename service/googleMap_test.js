var Maps = require("@google/maps")
const GOOGLE_MAPS_API_KEY =
  process.env.GOOGLE_MAPS_API_KEY || "AIzaSyCwKCXngqJzmsT-T0uKU7d2gRp4N-Eq6c0"
let googleMapsClient = Maps.createClient({
  key: GOOGLE_MAPS_API_KEY,
})

googleMapsClient.reverseGeocode(
  {
    latlng: [10.7424706, 106.6661534],
    result_type: ["administrative_area_level_3"],
    location_type: ["ROOFTOP", "APPROXIMATE"],
  },
  (err, res) => {
    console.log(res.requestUrl)
    console.log(res.json.results[0].address_components)
  }
)
