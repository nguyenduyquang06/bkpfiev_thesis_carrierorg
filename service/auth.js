import getConfig from "next/config"
import API, { serverGet } from "./base"

const login = async (username, password) => {
  try {
    const body = {
      username,
      password,
      orgName: "carrierorg",
    }
    const {
      data: { token, success, message },
    } = await API.post("/login", body)
    const { publicRuntimeConfig } = getConfig()

    // set cookie
    await fetch("/setCookie", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        token,
      }),
    })
    return {
      token,
      success,
      message,
    }
  } catch (err) {
    throw err
  }
}

const logout = async () => {
  try {
    const { data } = await API.get("/logout")
    return data
  } catch (err) {
    throw err
  }
}

const getCurrentUser = async context => {
  try {
    let resp = {}
    if (context && context.res) {
      resp = await serverGet("/", context)
    }
    return resp.data.user
  } catch (e) {
    throw e
  }
}

const checkAuthorized = async context => {
  try {
    let resp = {}
    if (context && context.res) {
      resp = await serverGet("/", context)
    }
    return resp.data.user
  } catch (e) {
    const statusCode = e.response.status
    switch (statusCode) {
      case 404:
        return true
      case 401:
        return false
    }
    throw e
  }
}

export { login, logout, getCurrentUser, checkAuthorized }
