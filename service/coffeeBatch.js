import { queryCC } from "./base"

const getAllCoffeeBatch = async (context = {}) => {
  try {
    let resp = {}
    resp = await queryCC("getAllCoffeeBatch", context)
    return resp.data
  } catch (e) {
    throw e
  }
}

export { getAllCoffeeBatch }
