import axios from "axios"
import getConfig from "next/config"
import parseCookies from "../lib/cookie-parser"

const { publicRuntimeConfig } = getConfig()

const CCVERSION = process.env.CCVERSION || "v0"

const API = axios.create({
  baseURL: publicRuntimeConfig.API_END_POINT,
  timeout: 60000,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
})

// axios object for render server
const SERVER = axios.create({
  baseURL: publicRuntimeConfig.API_END_POINT,
  timeout: 10000,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
})

/**
 * Get method for server call API
 * @param  {String} [url='']     Required. Endpoint, must start with /, ex: /auth/login
 * @param  {Object} [context={}] Required. Context is the context in getInitialProps(context)
 * @return {Promise<Response>}   Response or throw error
 */
export function serverGet(url = "", context = {}, params = {}) {
  // Get to add more cookie fields into request
  const { token } = parseCookies(context)

  return SERVER.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    data: params,
  })
}

export function queryCC(fcnName, context = {}, args = ["test"]) {
  // Get to add more cookie fields into request
  const { token } = parseCookies(context)
  const url = `/chaincode/${fcnName}?ccversion=${CCVERSION}&args=${JSON.stringify(
    args
  )}`
  return SERVER.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}

export function postCC(url = "", context = {}, params = {}) {
  // Get to add more cookie fields into request
  const { token } = parseCookies(context)

  return SERVER.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    data: params,
  })
}

export default API
