import Maps from "@google/maps";
import _ from "lodash";
const GOOGLE_MAPS_API_KEY =
  process.env.GOOGLE_MAPS_API_KEY || "AIzaSyA7Vzoo-bz-oYmSeSBqNkREYJ2flzSqZiA"

let googleMapsClient = Maps.createClient({
  key: GOOGLE_MAPS_API_KEY,
})

export const getLocationByAxis = (long, lat) => {
  googleMapsClient.reverseGeocode({
      latlng: [long, lat],
      result_type: ["administrative_area_level_3"],
      location_type: ["ROOFTOP", "APPROXIMATE"],
    },
    (err, res) => {
      // console.log(res.requestUrl)
      // console.log(res.json.results[0].address_components)
      console.log(err)
      console.log(_.get(res.json.results, "0", "address_components"))
    }
  )
}
