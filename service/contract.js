import { queryCC } from "./base"

const getAllContract = async (context = {}) => {
  try {
    let resp = {}
    resp = await queryCC("getAllContract", context)
    return resp.data
  } catch (e) {
    throw e
  }
}

const getAllContractStepOne = async (context = {}) => {
  try {
    let resp = {}
    resp = await queryCC("getAllContractStepOne", context)
    return resp.data
  } catch (e) {
    throw e
  }
}

const getAllContractStepTwo = async (context = {}) => {
  try {
    let resp = {}
    resp = await queryCC("getAllContractStepTwo", context)
    return resp.data
  } catch (e) {
    throw e
  }
}

const getAllContractStepThree = async (context = {}) => {
  try {
    let resp = {}
    resp = await queryCC("getAllContractStepThree", context)
    return resp.data
  } catch (e) {
    throw e
  }
}

export {
  getAllContract,
  getAllContractStepOne,
  getAllContractStepTwo,
  getAllContractStepThree,
}
