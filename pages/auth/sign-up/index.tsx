import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles"
import autobind from "autobind-decorator"
import loGet from "lodash/get"
import React from "react"
import { connect } from "react-redux"
import { bindActionCreators, Dispatch } from "redux"
import { Action } from "typescript-fsa"
import Checkout from "../../../components/SignupForm/Checkout"
import redirect from "../../../lib/redirect"
import { AuthActionImplement } from "../../../store/actions/AuthAction"
import { IInitialState } from "../../../store/states"

interface IProps extends WithStyles<typeof styles> {
  actions: {
    loginUser: (
      username: string,
      password: string
    ) => (dispatch: any) => Promise<any>
  }
  authReducer: object
}

interface IState {
  username: string
  password: string
  error: string
  open: boolean
}

const styles = (theme: Theme) =>
  createStyles({
    root: {},
    mainContainer: {
      padding: theme.spacing.unit * 2,
    },
    container: {
      display: "flex",
      flexWrap: "wrap",
      margin: "10px",
      paddingLeft: "30px",
      paddingRight: "30px",
    },
    textField: {
      color: "#FF4224",
      "&:before": {
        backgroundColor: "#FF4224",
      },
    },
    card: {
      borderRadius: "50px",
      maxWidth: "400px",
      maxHeight: "80%",
      margin: "0 auto",
      zIndex: 1,
    },
    cardActions: {
      paddingTop: "50px",
      margin: "0 auto",
      width: "70%",
    },
    button: {
      background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
      borderRadius: "3px",
      width: "100%",
      border: 0,
      color: "white",
      height: 40,
      fontWeight: "bold",
      margin: "0 auto",
      padding: "0 30px",
      boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
      "&:hover": {
        background: "linear-gradient(200deg, #FE6B8B 30%, #FF8E53 90%)",
      },
    },
    logo: {
      height: "80px",
      marginTop: "100px",
    },
    logoWrapper: {
      display: "flex",
      justifyContent: "center",
    },
    body: {
      height: "200px",
      display: "flex",
      alignItems: "center",
      background: "#f7f7f7",
    },
    backgroundLogin: {
      background:
        'url("/static/img/supply_chain_background.jpg") no-repeat center center',
      backgroundSize: "100%",
      position: "absolute",
      height: "100vh",
      // left: "50%",
      width: "500px",
    },
    error: {
      backgroundColor: "#fdd9d7",
      color: "#7f231c",
      boxShadow: "none",
    },
    icon: {
      fontSize: 20,
    },
    iconVariant: {
      opacity: 0.9,
      marginRight: "10px",
    },
    snackbarSpan: {
      display: "flex",
      alignItems: "center",
    },
    main: {
      width: "auto",
      display: "block", // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 400,
        marginLeft: "auto",
        marginRight: "auto",
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
        .spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
  })

@autobind
class LoginPage extends React.Component<IProps, IState> {
  /**
   * Initialize server side rendering
   */
  static async getInitialProps(ctx) {
    const isLoggedIn = loGet(ctx.store.getState(), "auth.isLoggedIn")
    if (isLoggedIn) {
      redirect(ctx, "/")
    }
    return {}
  }

  constructor(props: IProps) {
    super(props)
    this.state = {
      username: "",
      password: "",
      error: "",
      open: false,
    }
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  handleUserNameChange = event => {
    this.setState({
      username: event.target.value,
      error: "",
      open: false,
    })
  }

  handlePasswordChange = event => {
    this.setState({
      password: event.target.value,
      error: "",
      open: false,
    })
  }

  onSubmit = async event => {
    event.preventDefault()
    const { actions, authReducer } = this.props
    const { username, password } = this.state
    return new Promise((resolve, reject) => {
      resolve(actions.loginUser(username, password))
    }).then(res => {
      if (res.payload.success) {
        redirect(event, "/")
      }
    })
  }

  render() {
    const { classes } = this.props
    // const { username, password, error, open } = this.state
    return <Checkout />
  }
}

const mapStateToProps = (state: IInitialState) => {
  return {
    authReducer: state.auth,
  }
}

const mapDispatchToProps = (dispatch: Dispatch<Action<any>>) => {
  return {
    actions: bindActionCreators(AuthActionImplement, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(LoginPage as any))
