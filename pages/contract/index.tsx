import { Fab, Paper, Tooltip, Typography } from "@material-ui/core"
import CircularProgress from "@material-ui/core/CircularProgress"
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles"
import { Info, OpenInNew, Refresh } from "@material-ui/icons"
import autobind from "autobind-decorator"
import _ from "lodash"
import loGet from "lodash/get"
import React from "react"
import { connect } from "react-redux"
import ReactTable from "react-table"
import "react-table/react-table.css"
import { bindActionCreators, Dispatch } from "redux"
import { Action } from "typescript-fsa"
import CoffeeBatchTrackingDetailsDialog from "../../components/dialogs/CoffeeBatchTrackingDetailsDialog"
import ViewCoffeeBatchDetails from "../../components/dialogs/ViewCoffeeBatchDetails"
import { HeaderArticleContainer } from "../../components/organisms"
import { Layout } from "../../components/templates"
import { Page } from "../../constants"
import { getAllContract } from "../../service/contract"
import { getLocationByAxis } from "../../service/googleMap"
import { IPagePayload, PageActions } from "../../store/actions"
import { CBActionImplement } from "../../store/actions/CoffeeBatchAction"
import { ICoffeeBatchState, IInitialState } from "../../store/states"

const mappingStatusToValue = {
  contractStepOne: {
    REQUEST_FROM_EXPORTER: 1,
    ACCEPT_REQUEST_TRADE_STEP_ONE_AND_SHIPPING: 2,
    CARRIER_DONE_SHIPPING: 3,
    DONE_SUCCESSFULLY: 4,
  },
  contractStepTwo: {
    REQUEST_STEP_TWO_FROM_IMPORTER: 1,
    READY_SHIPPING_STEP_TWO: 2,
    SHIPPING_EXPORTER_IMPORTER: 3,
    CARRIER_DONE_SHIPPING: 3,
    DONE_SUCCESSFULLY: 4,
  },
  contractStepThree: {
    REQUEST_STEP_THREE_FROM_FACTORY: 1,
    ACCEPT_REQUEST_TRADE_STEP_THREE_AND_SHIPPING: 2,
    CARRIER_DONE_SHIPPING: 3,
    DONE_SUCCESSFULLY: 4,
  },
}

interface IProps extends WithStyles<typeof styles> {
  coffeeBatchReducer: ICoffeeBatchState
  changePage: (pagePayload: IPagePayload) => void
  actions: {
    getAllCoffeeBatchDPClient: () => Dispatch<any>
  }
}

const MockData = [
  {
    temperature: 32.5,
    humidity: 32.5,
    county: "Quan 10",
    state: "TP HCM",
    country: "Viet Nam",
    latitude: 10.7424999,
    longitude: 106.6682569,
    rfcTime: "2019-02-26T06:40:11+00:00",
  },
  {
    temperature: 32.5,
    humidity: 32.5,
    county: "Quan 10",
    state: "TP HCM",
    country: "Viet Nam",
    latitude: 10.7462148,
    longitude: 106.6688474,
    rfcTime: "2019-02-26T07:00:11+00:00",
  },
  {
    temperature: 32.5,
    humidity: 32.5,
    county: "Quan 10",
    state: "TP HCM",
    latitude: 10.7496584,
    longitude: 106.6690886,
    country: "Viet Nam",
    rfcTime: "2019-02-26T07:20:11+00:00",
  },
]

const getAllCoffeeBatchRender = list => {
  return _.map(list, o => {
    return {
      growerMSPID: o.growerMSPID,
      growerID: o.growerID,
      batchID: o.batchID,
      batchState: o.batchState,
      cropDate: o.cropDate,
      fromID: "DEVELOPING",
      toID: "DEVELOPING",
      actions: "DEVELOPING",
    }
  })
}

interface IState {
  showTrackingDialog: boolean
  showViewMore: boolean
  showRefreshBtn: boolean
  selectedCoffeeBatch: object
  data: Array<object>
}

const styles = (theme: Theme) =>
  createStyles({
    root: {},
    counter: {
      margin: 10,
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
    },
    mainContainer: {
      padding: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit,
    },
    title: {
      fontSize: "2em",
    },
    progress: {
      margin: theme.spacing.unit * 2,
    },
  })

@autobind
class Contract extends React.Component<IProps, IState> {
  /**
   * Initialize server side rendering
   */
  static getInitialProps = async ctx => {
    const pagePayload: IPagePayload = {
      selectedPage: Page.CONTRACT,
    }
    ctx.store.dispatch({
      type: PageActions.changePage.toString(),
      payload: pagePayload,
    })
    return {
      allContract: JSON.parse(loGet(await getAllContract(ctx), "message")),
    }
  }

  constructor(props: IProps) {
    super(props)
    const {
      coffeeBatchReducer: { coffeeBatchList },
      allContract,
    } = props
    console.log(allContract)
    this.state = {
      data: getAllCoffeeBatchRender(coffeeBatchList),
      showRefreshBtn: true,
      showViewMore: false,
      showTrackingDialog: false,
      selectedCoffeeBatch: coffeeBatchList[0] || [],
    }
  }

  static getDerivedStateFromProps(props, state) {
    return {
      data: getAllCoffeeBatchRender(props.coffeeBatchReducer.coffeeBatchList),
      showRefreshBtn: true,
    }
  }
  renderRefresh() {
    const { classes } = this.props

    if (this.state.showRefreshBtn) {
      return (
        <div align={"right"}>
          <Fab
            onClick={e => {
              this.handleRefresh()
            }}
            size="large"
            color="secondary"
          >
            <Refresh />
          </Fab>
        </div>
      )
    }
    return (
      <div align={"right"}>
        <CircularProgress className={classes.progress} color="secondary" />
      </div>
    )
  }

  handleAcceptTrade = () => {
    // getLocationByAxis(10.8585418, 106.7887749)
    getLocationByAxis(13.5726633, 113.0151106)
  }

  handleRefresh = async () => {
    const { actions } = this.props
    this.setState({ showRefreshBtn: false })
    await actions.getAllCoffeeBatchDPClient()
  }

  handleCloseViewMore = () => {
    this.setState({ showViewMore: false })
  }

  handleCloseTracking = () => {
    this.setState({ showTrackingDialog: false })
  }

  render() {
    const {
      classes,
      coffeeBatchReducer: { coffeeBatchList },
    } = this.props
    const { data, selectedCoffeeBatch, showTrackingDialog } = this.state
    return (
      <Layout>
        <HeaderArticleContainer>
          <Paper className={classes.mainContainer}>
            <Typography
              style={{ display: "inline" }}
              component={"span"}
              gutterBottom
              variant="h4"
              color="inherit"
            >
              List of Contracts
              {this.renderRefresh()}
            </Typography>
            <ReactTable
              data={data || []}
              columns={[
                {
                  Header: "Batch Information",
                  columns: [
                    {
                      Header: "Batch ID",
                      id: "batchID",
                      accessor: d => d.batchID,
                    },
                    {
                      Header: "Batch State",
                      id: "batchState",
                      accessor: d => d.batchState,
                    },
                    {
                      Header: "Progress",
                      id: "progress",
                      accessor: d => d.batchState,
                    },
                    {
                      Header: "From",
                      id: "fromID",
                      accessor: d => d.fromID,
                    },
                    {
                      Header: "To",
                      id: "toID",
                      accessor: d => d.toID,
                    },
                  ],
                },
                {
                  Header: "Coffee Storage",
                  columns: [
                    {
                      Header: "Temperature",
                      id: "growerMSPID",
                      accessor: d => d.growerMSPID,
                    },
                    {
                      Header: "Hudmidtiy",
                      id: "growerID",
                      accessor: d => d.growerID,
                    },
                    {
                      Header: "County",
                      id: "growerID",
                      accessor: d => d.growerID,
                    },
                    {
                      Header: "State",
                      id: "growerID",
                      accessor: d => d.growerID,
                    },
                    {
                      Header: "Country",
                      id: "growerID",
                      accessor: d => d.growerID,
                    },
                  ],
                },
                {
                  Header: "Interactive",
                  columns: [
                    {
                      Header: "Actions",
                      id: "actions",
                      accessor: d => d.actions,
                      Cell: row => {
                        return (
                          <div align="center">
                            <CoffeeBatchTrackingDetailsDialog
                              open={showTrackingDialog}
                              handleClose={this.handleCloseTracking}
                              coffeeBatch={MockData}
                            />
                            <ViewCoffeeBatchDetails
                              open={this.state.showViewMore}
                              coffeeBatch={selectedCoffeeBatch}
                              handleClose={() => {
                                this.handleCloseViewMore()
                              }}
                            />
                            <Tooltip title="View more details">
                              <Fab
                                size="small"
                                color="primary"
                                onClick={() => {
                                  this.setState({
                                    showViewMore: true,
                                    selectedCoffeeBatch: loGet(
                                      coffeeBatchList,
                                      row.index
                                    ),
                                  })
                                }}
                              >
                                <Info />
                              </Fab>
                            </Tooltip>
                            <Tooltip title="Open tracking dialogs">
                              <Fab
                                size="small"
                                color="primary"
                                onClick={() => {
                                  this.setState({
                                    showTrackingDialog: true,
                                    selectedCoffeeBatch: loGet(
                                      coffeeBatchList,
                                      row.index
                                    ),
                                  })
                                }}
                              >
                                <OpenInNew />
                              </Fab>
                            </Tooltip>
                          </div>
                        )
                      },
                    },
                  ],
                },
              ]}
              defaultPageSize={5}
              className="-striped -highlight"
            />
          </Paper>
        </HeaderArticleContainer>
      </Layout>
    )
  }
}

const mapStateToProps = (state: IInitialState) => {
  return {
    coffeeBatchReducer: state.coffeeBatch,
  }
}

const mapDispatchToProps = (dispatch: Dispatch<Action<any>>) => {
  return {
    actions: bindActionCreators(CBActionImplement, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Contract as any))
