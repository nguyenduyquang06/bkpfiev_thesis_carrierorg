import { Avatar, Paper, Typography } from "@material-ui/core"
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles"
import autobind from "autobind-decorator"
import React from "react"
import { connect } from "react-redux"
import { bindActionCreators, Dispatch } from "redux"
import { Action } from "typescript-fsa"
import { HeaderArticleContainer } from "../../components/organisms"
import { Layout } from "../../components/templates"
import { Page } from "../../constants"
import { IPagePayload, PageActions, UserAction } from "../../store/actions"
import { IInitialState } from "../../store/states"

interface IProps extends WithStyles<typeof styles> {
  count: number
  login: () => number
  logout: () => number
  getToken: () => number
  changePage: (pagePayload: IPagePayload) => void
}

interface IState {
  inputNumber: number
}

const styles = (theme: Theme) =>
  createStyles({
    root: {},
    counter: {
      margin: 10,
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
    },
    mainContainer: {
      padding: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit,
    },
    title: {
      fontSize: "2em",
    },
  })

@autobind
class User extends React.Component<IProps, IState> {
  /**
   * Initialize server side rendering
   */
  static getInitialProps = ctx => {
    const pagePayload: IPagePayload = {
      selectedPage: Page.USER,
    }
    ctx.store.dispatch({
      type: PageActions.changePage.toString(),
      payload: pagePayload,
    })
  }

  constructor(props: IProps) {
    super(props)
    this.state = {
      inputNumber: 12,
    }
  }

  /**
   * Increment count
   */
  handleLogin = () => this.props.login()

  /**
   * Decrement count
   */
  handleLogout = () => this.props.logout()

  handleGetToken = () => this.props.getToken()
  /**
   * Change inputNumber value
   */
  handleChangeCount = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.target.value
    // ignore not number
    if (val.match(/^([1-9]|0)+[0-9]*$/i)) {
      this.setState({ inputNumber: Number(val) })
    }
  }

  render() {
    const { classes, count } = this.props
    const CurrentNumber = () => (
      <Avatar className={classes.counter}>{count}</Avatar>
    )
    return (
      <Layout>
        <HeaderArticleContainer>
          <Paper className={classes.mainContainer}>
            <Typography variant="h2" gutterBottom className={classes.title}>
              Balance ETH
            </Typography>
          </Paper>
        </HeaderArticleContainer>
      </Layout>
    )
  }
}

const mapStateToProps = (state: IInitialState) => ({
  count: state.counter.count,
})

const mapDispatchToProps = (dispatch: Dispatch<Action<any>>) =>
  bindActionCreators(UserAction, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(User as any))
