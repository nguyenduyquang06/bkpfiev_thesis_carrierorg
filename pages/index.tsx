import { Paper, Typography } from "@material-ui/core"
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles"
import { HeaderArticleContainer } from "../components/organisms"
import { Layout } from "../components/templates"

const styles = (theme: Theme) =>
  createStyles({
    root: {},
    mainContainer: {
      padding: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit,
    },
  })

interface IProps extends WithStyles<typeof styles> {}

const Index = (props: IProps) => {
  const { classes } = props
  return (
    <Layout>
      <HeaderArticleContainer>
        <Paper className={classes.mainContainer}>
          <div align="center" style={{ padding: "20px" }}>
            <Typography variant="h3">CoffeeNet</Typography>
            <Typography variant="h5">
              <em>
                Powered by <strong>Hyperledger Fabric platform</strong>
              </em>
            </Typography>
          </div>
          <Typography variant="h5">
            <strong>Thesis:</strong> Optimizing supply chain, tracking and
            tamper proof product using Blockchain and IoT in Agriculture
          </Typography>
          <Typography variant="h5">
            <strong>Target Product:</strong> Coffee
          </Typography>
          <Typography variant="h5">
            <strong>Origin: </strong> Buon Me Thuoc, Tay Nguyen
          </Typography>
          <Typography variant="h5">
            <strong>Introduction: </strong>
          </Typography>
          <Typography variant="h6">
            <div className="blurb">
              <p>
                <em>CoffeeNet</em> is a general purpose supply chain solution
                built using the power of{" "}
                <a target="_blank" href="https://github.com/hyperledger/fabric">
                  Hyperledger Fabric's
                </a>{" "}
                blockchain technology. It maintains a distributed ledger that
                tracks both asset provenance and a timestamped history detailing
                how an asset was stored, handled, and transported.
              </p>
              <p>
                <em>CoffeeNet</em> demonstrates this unique technology with an
                illustrative example: tracking the provenance of coffee from
                crop stage to cup of drinks. One day an application like this
                could be used by restaurants, grocery stores, and their
                customers to ensure the coffee they purchase is ethically
                sourced and properly transported.
              </p>
              <p>
                To use <em>CoffeeNet</em>, create an account using the link in
                the navbar above. Once logged in, you will be able to add new
                coffee assets to the blockchain and track them with data like
                temperature or location. You will be able to authorize other
                "agents" on the blockchain to track this data as well, or even
                transfer ownership or possession of the coffee entirely. For the
                adventurous, these actions can also be accomplished directly
                with the REST API running on the <em>Supply Chain</em> server,
                perfect for automated IoT sensors.
              </p>
            </div>
          </Typography>
          <div style={{ display: "block", textAlign: "center" }}>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://docs.google.com/document/d/17f3IVbxTSemHl3pSGj7tYu1KjU3kWDSwEje0ORM2qO0/edit?fbclid=IwAR3thb0myX50tKVyy74Dpn3p_atAbKUqIr_h24BmMs_ilKwzwk-wZIXxPOQ"
            >
              <img
                style={{ maxWidth: "100%", height: "auto" }}
                src="/static/img/supply_chain_scheme.png"
              />
            </a>
          </div>
        </Paper>
      </HeaderArticleContainer>
    </Layout>
  )
}

export default withStyles(styles)(Index)
