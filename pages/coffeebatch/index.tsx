import { Fab, Grid, Paper, Tooltip, Typography } from "@material-ui/core"
import CircularProgress from "@material-ui/core/CircularProgress"
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles"
import { Clear, HowToReg, Info, Refresh } from "@material-ui/icons"
import autobind from "autobind-decorator"
import _ from "lodash"
import loGet from "lodash/get"
import React from "react"
import { connect } from "react-redux"
import ReactTable from "react-table"
import "react-table/react-table.css"
import { bindActionCreators, Dispatch } from "redux"
import { Action } from "typescript-fsa"
import ViewCoffeeBatchDetails from "../../components/dialogs/ViewCoffeeBatchDetails"
import { HeaderArticleContainer } from "../../components/organisms"
import { Layout } from "../../components/templates"
import { Page } from "../../constants"
import { IPagePayload, PageActions } from "../../store/actions"
import { CBActionImplement } from "../../store/actions/CoffeeBatchAction"
import { ICoffeeBatchState, IInitialState } from "../../store/states"
interface IProps extends WithStyles<typeof styles> {
  coffeeBatchReducer: ICoffeeBatchState
  changePage: (pagePayload: IPagePayload) => void
  actions: {
    getAllCoffeeBatchDPClient: () => Dispatch<any>
  }
}

const getAllCoffeeBatchRender = list => {
  return _.map(list, o => {
    return {
      growerMSPID: o.growerMSPID,
      growerID: o.growerID,
      batchID: o.batchID,
      batchState: o.batchState,
      cropDate: o.cropDate,
      fromID: "DEVELOPING",
      toID: "DEVELOPING",
      actions: "DEVELOPING",
    }
  })
}

interface IState {
  showViewMore: boolean
  showRefreshBtn: boolean
  selectedCoffeeBatch: object
  data: Array<object>
}

const styles = (theme: Theme) =>
  createStyles({
    root: {},
    counter: {
      margin: 10,
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
    },
    mainContainer: {
      padding: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit,
    },
    title: {
      fontSize: "2em",
    },
    progress: {
      margin: theme.spacing.unit * 2,
    },
  })

@autobind
class CoffeeBatch extends React.Component<IProps, IState> {
  /**
   * Initialize server side rendering
   */
  static getInitialProps = async ctx => {
    const pagePayload: IPagePayload = {
      selectedPage: Page.BATCH,
    }
    ctx.store.dispatch({
      type: PageActions.changePage.toString(),
      payload: pagePayload,
    })
    await CBActionImplement.getAllCoffeeBatchDispatch(ctx)
  }

  constructor(props: IProps) {
    super(props)
    const {
      coffeeBatchReducer: { coffeeBatchList },
    } = props
    this.state = {
      data: getAllCoffeeBatchRender(coffeeBatchList),
      showRefreshBtn: true,
      showViewMore: false,
      selectedCoffeeBatch: coffeeBatchList[0] || [],
    }
  }

  static getDerivedStateFromProps(props, state) {
    return {
      data: getAllCoffeeBatchRender(props.coffeeBatchReducer.coffeeBatchList),
      showRefreshBtn: true,
    }
  }
  renderRefresh() {
    const { classes } = this.props
    if (this.state.showRefreshBtn) {
      return (
        <div align={"right"}>
          <Fab
            onClick={e => {
              this.handleRefresh()
            }}
            size="large"
            color="secondary"
          >
            <Refresh />
          </Fab>
        </div>
      )
    }
    return (
      <div align={"right"}>
        <CircularProgress className={classes.progress} color="secondary" />
      </div>
    )
  }

  handleRefresh = async () => {
    const { actions } = this.props
    this.setState({ showRefreshBtn: false })
    await actions.getAllCoffeeBatchDPClient()
  }

  handleCloseViewMore = () => {
    this.setState({ showViewMore: false })
  }

  render() {
    const {
      classes,
      coffeeBatchReducer: { coffeeBatchList },
    } = this.props
    const { data, selectedCoffeeBatch } = this.state
    return (
      <Layout>
        <Grid container justify="center" alignItems="center" spacing={24}>
          <Grid item xs={12}>
            <HeaderArticleContainer>
              <Paper className={classes.mainContainer}>
                <Typography
                  style={{ display: "inline" }}
                  component={"span"}
                  variant="h4"
                  color="inherit"
                >
                  List of Coffee Batch
                  {this.renderRefresh()}
                </Typography>

                <Grid item xs={12}>
                  <ReactTable
                    data={data}
                    columns={[
                      {
                        Header: "Grower",
                        columns: [
                          {
                            Header: "MSPID",
                            id: "growerMSPID",
                            accessor: d => d.growerMSPID,
                          },
                          {
                            Header: "ID",
                            id: "growerID",
                            accessor: d => d.growerID,
                          },
                        ],
                      },
                      {
                        Header: "Batch Information",
                        columns: [
                          {
                            Header: "Batch ID",
                            id: "batchID",
                            accessor: d => d.batchID,
                          },
                          {
                            Header: "Batch State",
                            id: "batchState",
                            accessor: d => d.batchState,
                          },
                          {
                            Header: "Crop Date",
                            id: "cropDate",
                            accessor: d => d.cropDate,
                          },
                          {
                            Header: "From ID",
                            id: "fromID",
                            accessor: d => d.fromID,
                          },
                          {
                            Header: "To ID",
                            id: "toID",
                            accessor: d => d.toID,
                          },
                          {
                            Header: "Actions",
                            id: "actions",
                            accessor: d => d.actions,
                            Cell: row => {
                              return (
                                <div>
                                  <ViewCoffeeBatchDetails
                                    open={this.state.showViewMore}
                                    coffeeBatch={selectedCoffeeBatch}
                                    handleClose={() => {
                                      this.handleCloseViewMore()
                                    }}
                                  />
                                  <Tooltip title="Accept Request">
                                    <Fab size="small" color="primary">
                                      <HowToReg />
                                    </Fab>
                                  </Tooltip>
                                  <Tooltip title="Decline Request">
                                    <Fab size="small" color="secondary">
                                      <Clear />
                                    </Fab>
                                  </Tooltip>
                                  <Tooltip title="View more details">
                                    <Fab
                                      size="small"
                                      color="primary"
                                      onClick={() => {
                                        this.setState({
                                          showViewMore: true,
                                          selectedCoffeeBatch: loGet(
                                            coffeeBatchList,
                                            row.index
                                          ),
                                        })
                                      }}
                                    >
                                      <Info />
                                    </Fab>
                                  </Tooltip>
                                </div>
                              )
                            },
                          },
                        ],
                      },
                    ]}
                    defaultPageSize={5}
                    className="-striped -highlight"
                  />
                </Grid>

                <Typography component={"span"} variant="h4" color="inherit">
                  Summary
                </Typography>
                <Typography variant="h6">Total Coffee Batch:</Typography>
                <Typography variant="h6">Total Ative Coffee Batch:</Typography>
              </Paper>
            </HeaderArticleContainer>
          </Grid>
        </Grid>
      </Layout>
    )
  }
}

const mapStateToProps = (state: IInitialState) => {
  return {
    coffeeBatchReducer: state.coffeeBatch,
  }
}

const mapDispatchToProps = (dispatch: Dispatch<Action<any>>) => {
  return {
    actions: bindActionCreators(CBActionImplement, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(CoffeeBatch as any))
