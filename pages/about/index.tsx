import { Paper, Typography } from "@material-ui/core";
import { createStyles, Theme, withStyles, WithStyles } from "@material-ui/core/styles";
import autobind from "autobind-decorator";
import React from "react";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import { Action } from "typescript-fsa";
import { HeaderArticleContainer } from "../../components/organisms";
import { Layout } from "../../components/templates";
import { Page } from "../../constants";
import { IPagePayload, PageActions } from "../../store/actions";
const styles = (theme: Theme) =>
  createStyles({
    root: {},
    mainContainer: {
      padding: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit,
    },
  })

interface IProps extends WithStyles<typeof styles> {
  changePage: (pagePayload: IPagePayload) => void
}

interface IState {}

@autobind
class About extends React.Component<IProps, IState> {
  static getInitialProps = ctx => {
    const pagePayload: IPagePayload = {
      selectedPage: Page.ABOUT,
    }
    ctx.store.dispatch({
      type: PageActions.changePage.toString(),
      payload: pagePayload,
    })
  }

  constructor(props: IProps) {
    super(props)
  }

  render() {
    const { classes } = this.props
    return (
      <Layout>
        <HeaderArticleContainer>
          <Paper className={classes.mainContainer}>
            <Typography variant="h4">
              <strong>Contact Information</strong>
            </Typography>
            <div style={{ textAlign: "center", display: "block" }}>
              <Typography component={'span'} variant="h5">Nguyễn Duy Quang</Typography>
              <Typography component={'span'} variant="h5">+84932069973</Typography>
              <Typography component={'span'} variant="h5">Bach Khoa University</Typography>
              <Typography component={'span'} variant="h5">nguyenduyquang06@gmail.com</Typography>
            </div>
          </Paper>
        </HeaderArticleContainer>
      </Layout>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch<Action<IPagePayload>>) =>
  bindActionCreators(PageActions, dispatch)

export default connect(
  undefined,
  mapDispatchToProps
)(withStyles(styles)(About as any))
